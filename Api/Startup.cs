using System.Reflection;
using Api.Services;
using Api.Extensions;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Npgsql;

namespace Api
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{

			string postgreConnectionString = Configuration.GetConnectionString("Pgsql");
			services.AddDbContext<PgsqlContext>(options => options.UseNpgsql(postgreConnectionString));

			services.AddControllers();
			services.AddAutoMapper(typeof(Startup).GetTypeInfo().Assembly);
			services.AddMediatR(typeof(Startup).GetTypeInfo().Assembly);
			services.AddSerilogServices();
			services.AddTransient<NpgsqlConnection>((c) => new NpgsqlConnection(postgreConnectionString));
			services.AddSingleton<ErrorMessageService>();

		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseCors(builder => builder
				.AllowAnyHeader()
				.AllowAnyMethod()
				.SetIsOriginAllowed((host) => true)
				.AllowCredentials()
			);

			app.UseHttpsRedirection();

			app.UseRouting();

			app.UseAuthorization();

			// control to run seeders 
			bool runSeed = bool.Parse(Configuration.GetValue<string>("Seed"));
			
			app.UseSeed();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}

using System;
using System.ComponentModel.DataAnnotations;
using Api.Filters;

namespace Api.Models
{
	public class DeleteTravelModel
	{

		[Required]
		[NotEmpty]
		public Guid Id { get; set; }

	}
}

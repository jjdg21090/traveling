using System.ComponentModel.DataAnnotations;

namespace Api.Models
{
	public class UpdateTravelModel
	{
		[Required]
		public int Seats { get; set; }
		[Required]
		public string Origin { get; set; }
		[Required]
		public string Destination { get; set; }
		[Required]
		public double Price { get; set; }
	}
}

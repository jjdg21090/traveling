using System;
using System.ComponentModel.DataAnnotations;
using Api.Filters;

namespace Api.Models
{
	public class NewReservationModel
	{
		// validate if is 0-0-0-0  value
		[Required]
		[NotEmpty]
		public Guid TravelerId { get; set; }
		// validate if is 0-0-0-0  value
		[Required]
		[NotEmpty]
		public Guid TravelId { get; set; }
	}
}

using System.ComponentModel.DataAnnotations;

namespace Api.Models
{
	public class NewTravelerModel
	{
		[Required]
		public string Name { get; set; }
		[Required]
		public string DocumentId { get; set; }
		[Required]
		public string Address { get; set; }
		[Required]
		public string Phone { get; set; }
	}
}

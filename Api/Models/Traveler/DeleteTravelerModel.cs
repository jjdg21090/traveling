using System;
using System.ComponentModel.DataAnnotations;
using Api.Filters;

namespace Api.Models
{
	public class DeleteTravelerModel
	{

		[Required]
		[NotEmpty]
		public Guid Id { get; set; }

	}
}

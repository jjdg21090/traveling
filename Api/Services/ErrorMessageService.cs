﻿using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json.Linq;

namespace Api.Services
{
	public class ErrorMessageService
	{
		private readonly IWebHostEnvironment HostingEnvironment;
		private JObject JsonNotificationFile;

		public ErrorMessageService(IWebHostEnvironment hostingEnvironment)
		{
			HostingEnvironment = hostingEnvironment;
			string filePath = String.Concat(
				HostingEnvironment.ContentRootPath,
				Path.DirectorySeparatorChar,
				"Jsons",
				Path.DirectorySeparatorChar,
				"ErrorMessage.json"
			);
			JsonNotificationFile = JObject.Parse(File.ReadAllText(filePath));
		}

		public string GetErrorMessage(string Code, string Language)
		{
			if (String.IsNullOrEmpty(Language)) 
			{
				Language = "ENG";
			}
			JToken token = JsonNotificationFile.GetValue(Code).SelectToken(Language);
			if (String.IsNullOrEmpty(Language) || (token == null) || (token.Type == JTokenType.Null))
			{
				Language = "ENG";
			}
			string Error = JsonNotificationFile.GetValue(Code).SelectToken(Language).ToString();
			return Error;
		}
	}
}

using System;
using System.Reflection;

namespace Api.Extensions
{
	public static class ObjectExtensions
	{
		public static bool HasProperty(this Object obj,
			string propertyName)
		{
			Type type = Type.GetType(obj.GetType().ToString());
			PropertyInfo[] properties = type.GetProperties();
			PropertyInfo property = Array.Find(properties,
							element => element.Name == propertyName);

			return property == null ? false : true;
		}
	}
}

using System;

namespace Api.Extensions
{
	public static class SqlStringExtensions
	{
		public static string AddOrderBy(
			this string srt,
			string alias,
			string field,
			string direction
		)
		{
			if (string.IsNullOrEmpty(field))
			{
				return srt;
			}

			if (string.IsNullOrEmpty(srt))
			{
				srt = " ORDER BY ";
			}
			else
			{
				srt += ", ";
			}

			if (!string.IsNullOrEmpty(alias))
			{
				srt += String.Format("{0}.", alias);
			}

			srt += String.Format("\"{0}\"", field);

			if (!string.IsNullOrEmpty(direction))
			{
				srt += String.Format(" {0}", direction);
			}
			return srt;
		}
	}
}

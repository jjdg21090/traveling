using System.Linq;
using Api.Seeds;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Api.Extensions
{
	public static class SeedExtensions
	{
		public static async void UseSeed(this IApplicationBuilder app)
		{
			IServiceScope scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope();
			PgsqlContext context = scope.ServiceProvider.GetService<PgsqlContext>();
			IMediator mediator = scope.ServiceProvider.GetService<IMediator>();
			IConfiguration configuration = scope.ServiceProvider.GetService<IConfiguration>();

			using (context)
			{
				context.Database.Migrate();

				bool runSeed = bool.Parse(configuration.GetValue<string>("Seed"));
				if (runSeed)
				{

					// Here Goes call seeders

					if (!context.Travel.Any())
					{
						TravelSeed roleSeed = new TravelSeed(mediator);
						await roleSeed.Run();
					}
				}
			}
		}
	}
}

﻿using System;

namespace Api.Commands
{
	public interface ICommand
	{
		Guid Id { get; }
		DateTime DateRequested { get; }
	}
}

﻿using System;

namespace Api.Commands
{
	public class CommandBase : ICommand
	{
		protected CommandBase()
		{
			DateRequested = DateTime.UtcNow;
			Id = Guid.NewGuid();
		}

		public DateTime DateRequested
		{
			get; private set;
		}

		public Guid Id
		{
			get; private set;
		}
	}
}

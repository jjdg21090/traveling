using System;
using System.Threading;
using System.Threading.Tasks;
using Api.Entities;
using AutoMapper;
using MediatR;
using Serilog;

namespace Api.Commands
{
	public class CreateReservationHandler : IRequestHandler<CreateReservation, bool>
	{
		private readonly ILogger Logger;
		private readonly PgsqlContext DbContext;
		private readonly IMapper Mapper;

		public CreateReservationHandler(ILogger logger, IMapper mapper, PgsqlContext dbContext)
		{
			Logger = logger;
			DbContext = dbContext;
			Mapper = mapper;
		}

		public async Task<bool> Handle(CreateReservation message, CancellationToken token)
		{
			try
			{
				Reservation newReservation = new Reservation()
				{
					TravelerId = message.TravelerId,
					TravelId = message.TravelId
				};
				await DbContext.Reservation.AddAsync(newReservation);
				if (await DbContext.SaveChangesAsync() > 0)
				{
					return true;
				}
				return false;
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				throw new Exception("COMMAND_EXCEPTION", exception);
			}
		}
	}

	public class CreateReservation : CommandBase, IRequest<bool>
	{
		public readonly Guid TravelerId;
		public readonly Guid TravelId;

		public CreateReservation(
			string travelerId,
			string travelId
		)
		{
			TravelerId = Guid.Parse(travelerId);
			TravelId = Guid.Parse(travelId);
		}
	}
}
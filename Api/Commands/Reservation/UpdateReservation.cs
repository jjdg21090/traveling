using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Api.Entities;
using MediatR;
using Serilog;

namespace Api.Commands
{
	public class UpdateReservationHandler : IRequestHandler<UpdateReservation, bool>
	{
		private readonly ILogger Logger;
		private readonly PgsqlContext DbContext;

		public UpdateReservationHandler(ILogger logger, PgsqlContext dbContext)
		{
			Logger = logger;
			DbContext = dbContext;
		}

		public async Task<bool> Handle(UpdateReservation message, CancellationToken token)
		{
			try
			{
				Reservation Reservation = DbContext.Reservation
													.Where(c => c.ReservationId == message.ReservationId)
													.FirstOrDefault();

				if (Reservation != null)
				{
					Reservation.TravelerId = message.TravelerId;
					Reservation.TravelId = message.TravelId;

					DbContext.Update(Reservation);

					int resultSave = await DbContext.SaveChangesAsync();
					if (resultSave > 0)
					{
						return true;
					}
				}
				return false;
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				throw new Exception("COMMAND_EXCEPTION", exception);
			}
		}
	}

	public class UpdateReservation : CommandBase, IRequest<bool>
	{
		public readonly Guid ReservationId;
		public readonly Guid TravelerId;
		public readonly Guid TravelId;

		public UpdateReservation(
			string reservationId,
			string travelerId,
			string travelId
		)
		{
			ReservationId = Guid.Parse(reservationId);
			TravelerId = Guid.Parse(travelerId);
			TravelId = Guid.Parse(travelId);
		}
	}
}

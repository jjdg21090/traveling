using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Api.Entities;
using Serilog;

namespace Api.Commands
{
	public class DeleteReservationHandler : IRequestHandler<DeleteReservation, bool>
	{
		private readonly ILogger Logger;
		private readonly PgsqlContext DbContext;

		public DeleteReservationHandler(ILogger logger, PgsqlContext dbContext)
		{
			Logger = logger;
			DbContext = dbContext;
		}

		public async Task<bool> Handle(DeleteReservation message, CancellationToken token)
		{
			try
			{
				Reservation Reservation = DbContext.Reservation.Where(e => e.ReservationId == message.ReservationId).FirstOrDefault();
				DbContext.Reservation.Remove(Reservation);
				if (await DbContext.SaveChangesAsync() > 0)
				{
					return true;
				}
				return false;
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				throw new Exception("COMMAND_EXCEPTION", exception);
			}
		}
	}

	public class DeleteReservation : CommandBase, IRequest<bool>
	{
		public readonly Guid ReservationId;

		public DeleteReservation(string travelId)
		{
			ReservationId = Guid.Parse(travelId);
		}
	}
}

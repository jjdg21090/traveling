using MediatR;
using Api.Entities;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Api.Commands
{
	public class CreateTravelerHandler : IRequestHandler<CreateTraveler, bool>
	{
		private readonly ILogger Logger;
		private readonly PgsqlContext DbContext;

		public CreateTravelerHandler(ILogger logger, PgsqlContext dbContext)
		{
			Logger = logger;
			DbContext = dbContext;
		}

		public async Task<bool> Handle(CreateTraveler message, CancellationToken token)
		{
			try
			{
				Traveler Traveler = new Traveler()
				{
					Name = message.Name,
					DocumentId = message.DocumentId,
					Address = message.Address,
					Phone = message.Phone
				};
				DbContext.Traveler.Add(Traveler);
				if (await DbContext.SaveChangesAsync() > 0)
				{
					return true;
				}
				return false;
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				throw new Exception("COMMAND_EXCEPTION", exception);
			}
		}
	}

	public class CreateTraveler : CommandBase, IRequest<bool>
	{
		public readonly string Name;
		public readonly string DocumentId;
		public readonly string Address;
		public readonly string Phone;

		public CreateTraveler(
			string name,
			string documentId,
			string address,
			string phone
		)
		{
			Name = name;
			DocumentId = documentId;
			Address = address;
			Phone = phone;
		}
	}
}
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Api.Entities;
using Serilog;

namespace Api.Commands
{
	public class DeleteTravelerHandler : IRequestHandler<DeleteTraveler, bool>
	{
		private readonly ILogger Logger;
		private readonly PgsqlContext DbContext;

		public DeleteTravelerHandler(ILogger logger, PgsqlContext dbContext)
		{
			Logger = logger;
			DbContext = dbContext;
		}

		public async Task<bool> Handle(DeleteTraveler message, CancellationToken token)
		{
			try
			{
				Traveler Traveler = DbContext.Traveler.Where(e => e.TravelerId == message.TravelerId).FirstOrDefault();
				DbContext.Traveler.Remove(Traveler);
				if (await DbContext.SaveChangesAsync() > 0)
				{
					return true;
				}
				return false;
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				throw new Exception("COMMAND_EXCEPTION", exception);
			}
		}
	}

	public class DeleteTraveler : CommandBase, IRequest<bool>
	{
		public readonly Guid TravelerId;

		public DeleteTraveler(string travelId)
		{
			TravelerId = Guid.Parse(travelId);
		}
	}
}

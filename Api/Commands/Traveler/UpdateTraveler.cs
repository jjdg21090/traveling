using MediatR;
using Api.Entities;
using Serilog;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Api.Commands
{
	public class UpdateTravelerHandler : IRequestHandler<UpdateTraveler, bool>
	{
		private readonly ILogger Logger;
		private readonly PgsqlContext DbContext;

		public UpdateTravelerHandler(ILogger logger, PgsqlContext dbContext)
		{
			Logger = logger;
			DbContext = dbContext;
		}

		public async Task<bool> Handle(UpdateTraveler message, CancellationToken token)
		{
			try
			{
				Traveler Traveler = DbContext.Traveler.Where(c => c.TravelerId == message.TravelerId).FirstOrDefault();
				if (Traveler != null)
				{
					Traveler.Name = message.Name;
					Traveler.DocumentId = message.DocumentId;
					Traveler.Address = message.Address;
					Traveler.Phone = message.Phone;

					DbContext.Update(Traveler);

					int resultSave = await DbContext.SaveChangesAsync();
					if (resultSave > 0)
					{
						return true;
					}
				}
				return false;
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				throw new Exception("COMMAND_EXCEPTION", exception);
			}
		}
	}

	public class UpdateTraveler : CommandBase, IRequest<bool>
	{
		public readonly Guid TravelerId;
		public readonly string Name;
		public readonly string DocumentId;
		public readonly string Address;
		public readonly string Phone;

		public UpdateTraveler(
			string travelerId,
			string name,
			string documentId,
			string address,
			string phone
		)
		{
			TravelerId = Guid.Parse(travelerId);
			Name = name;
			DocumentId = documentId;
			Address = address;
			Phone = phone;
		}
	}
}

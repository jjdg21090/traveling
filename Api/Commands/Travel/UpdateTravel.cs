using MediatR;
using Api.Entities;
using Serilog;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Api.Commands
{
	public class UpdateTravelHandler : IRequestHandler<UpdateTravel, bool>
	{
		private readonly ILogger Logger;
		private readonly PgsqlContext DbContext;

		public UpdateTravelHandler(ILogger logger, PgsqlContext dbContext)
		{
			Logger = logger;
			DbContext = dbContext;
		}

		public async Task<bool> Handle(UpdateTravel message, CancellationToken token)
		{
			try
			{
				Travel Travel = DbContext.Travel.Where(c => c.TravelId == message.TravelId).FirstOrDefault();
				if (Travel != null)
				{
					Travel.Origin = message.Origin;
					Travel.Destination = message.Destination;
					Travel.Seats = message.Seats;
					Travel.Price = message.Price;

					DbContext.Update(Travel);

					int resultSave = await DbContext.SaveChangesAsync();
					if (resultSave > 0)
					{
						return true;
					}
				}
				return false;
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				throw new Exception("COMMAND_EXCEPTION", exception);
			}
		}
	}

	public class UpdateTravel : CommandBase, IRequest<bool>
	{
		public readonly Guid TravelId;
		public readonly string Origin;
		public readonly string Destination;
		public readonly int Seats;
		public readonly double Price;
		public readonly Guid TravelCategoryId;

		public UpdateTravel(
			string travelId,
			string origin,
			string destination,
			int seats,
			double price
		)
		{
			TravelId = Guid.Parse(travelId);
			Origin = origin;
			Destination = destination;
			Seats = seats;
			Price = price;
		}
	}
}

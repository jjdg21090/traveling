using MediatR;
using Api.Entities;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Api.Commands
{
	public class CreateTravelHandler : IRequestHandler<CreateTravel, bool>
	{
		private readonly ILogger Logger;
		private readonly PgsqlContext DbContext;

		public CreateTravelHandler(ILogger logger, PgsqlContext dbContext)
		{
			Logger = logger;
			DbContext = dbContext;
		}

		public async Task<bool> Handle(CreateTravel message, CancellationToken token)
		{
			try
			{
				Travel Travel = new Travel()
				{
					Origin = message.Origin,
					Destination = message.Destination,
					Seats = message.Seats,
					Price = message.Price
				};
				DbContext.Travel.Add(Travel);
				if (await DbContext.SaveChangesAsync() > 0)
				{
					return true;
				}
				return false;
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				throw new Exception("COMMAND_EXCEPTION", exception);
			}
		}
	}

	public class CreateTravel : CommandBase, IRequest<bool>
	{
		public readonly string Origin;
		public readonly string Destination;
		public readonly int Seats;
		public readonly double Price;

		public CreateTravel(
			string origin,
			string destination,
			int seats,
			double price
		)
		{
			Origin = origin;
			Destination = destination;
			Seats = seats;
			Price = price;
		}
	}
}
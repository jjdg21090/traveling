using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Api.Entities;
using Serilog;

namespace Api.Commands
{
	public class DeleteTravelHandler : IRequestHandler<DeleteTravel, bool>
	{
		private readonly ILogger Logger;
		private readonly PgsqlContext DbContext;

		public DeleteTravelHandler(ILogger logger, PgsqlContext dbContext)
		{
			Logger = logger;
			DbContext = dbContext;
		}

		public async Task<bool> Handle(DeleteTravel message, CancellationToken token)
		{
			try
			{
				Travel Travel = DbContext.Travel.Where(e => e.TravelId == message.TravelId).FirstOrDefault();
				DbContext.Travel.Remove(Travel);
				if (await DbContext.SaveChangesAsync() > 0)
				{
					return true;
				}
				return false;
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				throw new Exception("COMMAND_EXCEPTION", exception);
			}
		}
	}

	public class DeleteTravel : CommandBase, IRequest<bool>
	{
		public readonly Guid TravelId;

		public DeleteTravel(string travelId)
		{
			TravelId = Guid.Parse(travelId);
		}
	}
}

using Api.DTOs;
using Api.Entities;
using AutoMapper;
using Dapper;
using MediatR;
using Npgsql;
using Serilog;
using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Api.Queries
{
	public class GetTravelByIdHandler : IRequestHandler<GetTravelById, TravelDTO>
	{
		private readonly ILogger Logger;
		private readonly IMapper Mapper;
		private readonly IDbConnection DbConnection;

		public GetTravelByIdHandler(ILogger logger, IMapper mapper, NpgsqlConnection conection)
		{
			Logger = logger;
			Mapper = mapper;
			DbConnection = conection;
		}

		public async Task<TravelDTO> Handle(GetTravelById message, CancellationToken cancellationToken)
		{
			try
			{
				string sql = "SELECT * FROM \"Travel\" WHERE \"TravelId\" = @TravelId;";

				Travel result = await DbConnection.QueryFirstOrDefaultAsync<Travel>(sql, new { @TravelId = message.TravelId });

				if (result == null)
				{
					return null;
				}

				TravelDTO balanceCharge = Mapper.Map<TravelDTO>(result);
				return balanceCharge;
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				throw new Exception("QUERY_EXCEPTION", exception);
			}
		}
	}
	public class GetTravelById : IRequest<TravelDTO>
	{
		public readonly Guid TravelId;

		public GetTravelById(string travelId)
		{
			TravelId = Guid.Parse(travelId);
		}
	}
}
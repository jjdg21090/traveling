using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Dapper;
using MediatR;
using Npgsql;
using Api.DTOs;
using Api.Entities;
using Api.Extensions;
using Serilog;

namespace Api.Queries
{
	public class GetTravelsHandler : IRequestHandler<GetTravels, List<TravelDTO>>
	{
		private readonly ILogger Logger;
		private readonly IMapper Mapper;
		private readonly IDbConnection DbConnection;

		public GetTravelsHandler(ILogger logger, IMapper mapper, NpgsqlConnection conection)
		{
			Logger = logger;
			DbConnection = conection;
			Mapper = mapper;
		}

		public async Task<List<TravelDTO>> Handle(GetTravels message, CancellationToken cancellationToken)
		{
			try
			{
				string sql = "SELECT Tra.* FROM \"Travel\" Tra;";

				IEnumerable<Travel> TravelsResult = await DbConnection.QueryAsync<Travel>(sql);

				if (TravelsResult.Any())
				{
					List<TravelDTO> Travels = Mapper.Map<List<TravelDTO>>(TravelsResult);
					return Travels;
				}
				return null;
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				throw new Exception("QUERY_EXCEPTION", exception);
			}
		}
	}

	public class GetTravels : IRequest<List<TravelDTO>>
	{
		public readonly Dictionary<string, string> Parameters;

		public GetTravels(Dictionary<string, string> parameters = null)
		{
			Parameters = parameters;
		}
	}
}

using Api.DTOs;
using Api.Entities;
using AutoMapper;
using Dapper;
using MediatR;
using Npgsql;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Api.Queries
{
	public class GetReservationByIdHandler : IRequestHandler<GetReservationById, ReservationDTO>
	{
		private readonly ILogger Logger;
		private readonly IMapper Mapper;
		private readonly IDbConnection DbConnection;

		public GetReservationByIdHandler(ILogger logger, IMapper mapper, NpgsqlConnection conection)
		{
			Logger = logger;
			Mapper = mapper;
			DbConnection = conection;
		}

		public async Task<ReservationDTO> Handle(GetReservationById message, CancellationToken cancellationToken)
		{
			try
			{

				string sql = "SELECT Res.\"ReservationId\", Res.*, Tra.*, Tra2.*";
						sql += " FROM \"Reservation\" Res";
						sql += " JOIN \"Travel\" Tra ON Res.\"TravelId\" = Tra.\"TravelId\"";
						sql += " JOIN \"Traveler\" Tra2 ON Tra2.\"TravelerId\" = Res.\"TravelerId\"";
						sql += " WHERE Res.\"ReservationId\" = @ReservationId";

				IEnumerable<Reservation> resultTask = await DbConnection.
													QueryAsync<Reservation, Travel, Traveler, Reservation>(sql,
														(reservation, travel, traveler) =>
														{
															reservation.Travel = travel;
															reservation.TravelId = travel.TravelId;
															reservation.Traveler = traveler;
															reservation.TravelerId = traveler.TravelerId;

															return reservation;
														},
														new { @ReservationId = message.ReservationId },
														splitOn: "ReservationId,TravelId,TravelerId"
													);

				List<Reservation> result = resultTask.Distinct().ToList();

				if (result.Any())
				{
					ReservationDTO reservation = Mapper.Map<ReservationDTO>(result.FirstOrDefault());
					return reservation;
				}

				return null;
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				throw new Exception("QUERY_EXCEPTION", exception);
			}
		}
	}
	public class GetReservationById : IRequest<ReservationDTO>
	{
		public readonly Guid ReservationId;

		public GetReservationById(string reservationId)
		{
			ReservationId = Guid.Parse(reservationId);
		}
	}
}
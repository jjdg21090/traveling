using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Api.DTOs;
using Api.Entities;
using AutoMapper;
using Dapper;
using MediatR;
using Npgsql;
using Serilog;

namespace Api.Queries
{
	public class GetReservationsByTravelIdHandler : IRequestHandler<GetReservationsByTravelId, List<ReservationDTO>>
	{
		private readonly ILogger Logger;
		private readonly IMapper Mapper;
		private readonly IDbConnection DbConnection;

		public GetReservationsByTravelIdHandler(ILogger logger, IMapper mapper, NpgsqlConnection conection)
		{
			Logger = logger;
			DbConnection = conection;
			Mapper = mapper;
		}

		public async Task<List<ReservationDTO>> Handle(GetReservationsByTravelId message, CancellationToken cancellationToken)
		{
			try
			{

				string sql = "SELECT Res.\"ReservationId\", Res.*, Tra.*, Tra2.*";
						sql += " FROM \"Traveler\" Tra";
						sql += " JOIN \"Reservation\" Res ON Res.\"TravelerId\" = Tra.\"TravelerId\"";
						sql += " JOIN \"Travel\" Tra2 ON Tra2.\"TravelId\" = Res.\"TravelId\"";
						sql += " WHERE Tra2.\"TravelId\" = @TravelId";

				IEnumerable<Reservation> result = await DbConnection.
													QueryAsync<Reservation, Traveler, Travel, Reservation>(sql,
														(reservation, traveler, travel) =>
														{
															reservation.Traveler = traveler;
															reservation.Travel = travel;

															return reservation;
														},
														new { @TravelId = message.TravelId },
														splitOn: "ReservationId,TravelerId,TravelId"
													);

				if (result.Any())
				{
					List<ReservationDTO> reservations = Mapper.Map<List<ReservationDTO>>(result.Distinct());
					return reservations;
				}
				return null;
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				throw new Exception("QUERY_EXCEPTION", exception);
			}
		}
	}

	public class GetReservationsByTravelId : IRequest<List<ReservationDTO>>
	{
		public readonly Guid TravelId;

		public GetReservationsByTravelId(string travelId)
		{
			TravelId = Guid.Parse(travelId);
		}
	}
}

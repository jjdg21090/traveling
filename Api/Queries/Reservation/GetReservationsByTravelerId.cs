using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Api.DTOs;
using Api.Entities;
using AutoMapper;
using Dapper;
using MediatR;
using Npgsql;
using Serilog;

namespace Api.Queries
{
	public class GetReservationsByTravelerIdHandler : IRequestHandler<GetReservationsByTravelerId, List<ReservationDTO>>
	{
		private readonly ILogger Logger;
		private readonly IMapper Mapper;
		private readonly IDbConnection DbConnection;

		public GetReservationsByTravelerIdHandler(ILogger logger, IMapper mapper, NpgsqlConnection conection)
		{
			Logger = logger;
			DbConnection = conection;
			Mapper = mapper;
		}

		public async Task<List<ReservationDTO>> Handle(GetReservationsByTravelerId message, CancellationToken cancellationToken)
		{
			try
			{

				string sql = "SELECT Res.\"ReservationId\", Res.*, Tra.*, Tra2.*";
						sql += " FROM \"Travel\" Tra";
						sql += " JOIN \"Reservation\" Res ON Res.\"TravelId\" = Tra.\"TravelId\"";
						sql += " JOIN \"Traveler\" Tra2 ON Tra2.\"TravelerId\" = Res.\"TravelerId\"";
						sql += " WHERE Tra2.\"TravelerId\" = @TravelerId";

				IEnumerable<Reservation> result = await DbConnection.
													QueryAsync<Reservation, Travel, Traveler, Reservation>(sql,
														(reservation, travel, traveler) =>
														{
															reservation.Travel = travel;
															reservation.Traveler = traveler;

															return reservation;
														},
														new { @TravelerId = message.TravelerId },
														splitOn: "ReservationId,TravelId,TravelerId"
													);

				if (result.Any())
				{
					List<ReservationDTO> reservation = Mapper.Map<List<ReservationDTO>>(result.Distinct());
					return reservation;
				}
				return null;
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				throw new Exception("QUERY_EXCEPTION", exception);
			}
		}
	}

	public class GetReservationsByTravelerId : IRequest<List<ReservationDTO>>
	{
		public readonly Guid TravelerId;

		public GetReservationsByTravelerId(string travelerId)
		{
			TravelerId = Guid.Parse(travelerId);
		}
	}
}

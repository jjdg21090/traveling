using Api.DTOs;
using Api.Entities;
using AutoMapper;
using Dapper;
using MediatR;
using Npgsql;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Api.Queries
{
	public class GetReservationByTravelIdAndTravelerIdHandler : IRequestHandler<GetReservationByTravelIdAndTravelerId, ReservationDTO>
	{
		private readonly ILogger Logger;
		private readonly IMapper Mapper;
		private readonly IDbConnection DbConnection;

		public GetReservationByTravelIdAndTravelerIdHandler(ILogger logger, IMapper mapper, NpgsqlConnection conection)
		{
			Logger = logger;
			Mapper = mapper;
			DbConnection = conection;
		}

		public async Task<ReservationDTO> Handle(GetReservationByTravelIdAndTravelerId message, CancellationToken cancellationToken)
		{
			try
			{

				string sql = "SELECT Res.\"ReservationId\", Res.*";
						sql += " FROM \"Reservation\" Res";
						sql += " JOIN \"Travel\" Tra ON Res.\"TravelId\" = Tra.\"TravelId\"";
						sql += " JOIN \"Traveler\" Tra2 ON Tra2.\"TravelerId\" = Res.\"TravelerId\"";
						sql += " WHERE Res.\"TravelId\" = @TravelId AND Res.\"TravelerId\" = @TravelerId";

				IEnumerable<Reservation> resultTask = await DbConnection
													.QueryAsync<Reservation>(sql,
														new { @TravelId = message.TravelId, @TravelerId = message.TravelerId }
													);

				List<Reservation> result = resultTask.Distinct().ToList();

				if (result.Any())
				{
					ReservationDTO reservation = Mapper.Map<ReservationDTO>(result.FirstOrDefault());
					return reservation;
				}

				return null;
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				throw new Exception("QUERY_EXCEPTION", exception);
			}
		}
	}
	public class GetReservationByTravelIdAndTravelerId : IRequest<ReservationDTO>
	{
		public readonly Guid TravelId;
		public readonly Guid TravelerId;

		public GetReservationByTravelIdAndTravelerId(
			string travelId,
			string travelerId
		)
		{
			TravelId = Guid.Parse(travelId);
			TravelerId = Guid.Parse(travelerId);
		}
	}
}
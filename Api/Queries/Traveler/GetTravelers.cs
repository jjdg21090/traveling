using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Dapper;
using MediatR;
using Npgsql;
using Api.DTOs;
using Api.Entities;
using Api.Extensions;
using Serilog;

namespace Api.Queries
{
	public class GetTravelersHandler : IRequestHandler<GetTravelers, List<TravelerDTO>>
	{
		private readonly ILogger Logger;
		private readonly IMapper Mapper;
		private readonly IDbConnection DbConnection;

		public GetTravelersHandler(ILogger logger, IMapper mapper, NpgsqlConnection conection)
		{
			Logger = logger;
			DbConnection = conection;
			Mapper = mapper;
		}

		public async Task<List<TravelerDTO>> Handle(GetTravelers message, CancellationToken cancellationToken)
		{
			try
			{
				string sql = "SELECT Tra.* FROM \"Traveler\" Tra;";

				IEnumerable<Traveler> TravelersResult = await DbConnection.QueryAsync<Traveler>(sql);

				if (TravelersResult.Any())
				{
					List<TravelerDTO> Travelers = Mapper.Map<List<TravelerDTO>>(TravelersResult);
					return Travelers;
				}
				return null;
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				throw new Exception("QUERY_EXCEPTION", exception);
			}
		}
	}

	public class GetTravelers : IRequest<List<TravelerDTO>>
	{

		public GetTravelers() { }
	}
}

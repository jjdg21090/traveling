using Serilog;
using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Dapper;
using MediatR;
using Npgsql;
using Api.DTOs;
using Api.Entities;

namespace Api.Queries
{
	public class GetTravelerByIdHandler : IRequestHandler<GetTravelerById, TravelerDTO>
	{
		private readonly ILogger Logger;
		private readonly IMapper Mapper;
		private readonly IDbConnection DbConnection;

		public GetTravelerByIdHandler(ILogger logger, IMapper mapper, NpgsqlConnection conection)
		{
			Logger = logger;
			Mapper = mapper;
			DbConnection = conection;
		}

		public async Task<TravelerDTO> Handle(GetTravelerById message, CancellationToken cancellationToken)
		{
			try
			{
				string sql = "SELECT * FROM \"Traveler\" WHERE \"TravelerId\" = @TravelerId;";

				Traveler result = await DbConnection.QueryFirstOrDefaultAsync<Traveler>(sql, new { @TravelerId = message.TravelerId });

				if (result == null)
				{
					return null;
				}

				TravelerDTO balanceCharge = Mapper.Map<TravelerDTO>(result);
				return balanceCharge;
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				throw new Exception("QUERY_EXCEPTION", exception);
			}

		}
	}
	public class GetTravelerById : IRequest<TravelerDTO>
	{
		public readonly Guid TravelerId;

		public GetTravelerById(string travelId)
		{
			TravelerId = Guid.Parse(travelId);
		}
	}
}
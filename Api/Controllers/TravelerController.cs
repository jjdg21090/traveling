using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Commands;
using Api.DTOs;
using Api.Models;
using Api.Queries;
using Api.Services;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Serilog;

namespace Api.Controllers
{

	[Produces("application/json")]
	public class TravelerController : ControllerBase
	{
		private readonly ILogger Logger;
		private readonly ErrorMessageService ErrorMessageService;
		private readonly IMapper Mapper;
		private readonly IMediator Mediator;

		public TravelerController(ILogger iLogger, ErrorMessageService errorMessageService, IMapper mapper, IMediator mediator)
		{
			Logger = iLogger;
			ErrorMessageService = errorMessageService;
			Mapper = mapper;
			Mediator = mediator;
		}

		[HttpPost, Route("/Traveler")]
		public async Task<IActionResult> CreateTraveler([FromBody] NewTravelerModel model, [FromQuery] string language)
		{
			Object meta;
			try
			{

				if (!ModelState.IsValid)
				{
					List<string> errors = new List<string>();
					foreach (KeyValuePair<string, ModelStateEntry> state in ModelState)
					{
						foreach (ModelError error in state.Value.Errors)
						{
							errors.Add(error.ErrorMessage);
						}
					}
					meta = new
					{
						Error = new { Code = 101, Description = ErrorMessageService.GetErrorMessage("101", language), Data = errors }
					};
					return BadRequest(new { Meta = meta });
				}

				CreateTraveler newTraveler = new CreateTraveler(model.Name, model.DocumentId, model.Address, model.Phone);
				if (await Mediator.Send(newTraveler))
				{
					return Ok();
				}
				meta = new
				{
					Error = new { Code = 103, Description = ErrorMessageService.GetErrorMessage("103", language) }
				};
				return BadRequest(new { Meta = meta });
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				meta = new { Error = new { Code = 100, Description = ErrorMessageService.GetErrorMessage("100", language) } };
				return StatusCode(500, new { Meta = meta });
			}
		}

		[HttpPut, Route("/Traveler/{TravelerId}")]
		public async Task<IActionResult> UpdateTraveler([FromRoute] string TravelerId, [FromBody] UpdateTravelerModel model, [FromQuery] string language)
		{
			Object meta;
			try
			{
				if (!ModelState.IsValid)
				{
					List<string> errors = new List<string>();
					foreach (KeyValuePair<string, ModelStateEntry> state in ModelState)
					{
						foreach (ModelError error in state.Value.Errors)
						{
							errors.Add(error.ErrorMessage);
						}
					}
					meta = new
					{
						Error = new { Code = 101, Description = ErrorMessageService.GetErrorMessage("101", language), Data = errors }
					};
					return BadRequest(new { Meta = meta });
				}

				TravelerDTO Traveler = await Mediator.Send(new GetTravelerById(TravelerId));
				if (Traveler == null)
				{
					meta = new
					{
						Error = new { Code = 401, Description = ErrorMessageService.GetErrorMessage("401", language) }
					};
					return BadRequest(new { Meta = meta });
				}

				UpdateTraveler updateTraveler = new UpdateTraveler(
					TravelerId,
					model.Name,
					model.DocumentId,
					model.Address,
					model.Phone
				);

				if (await Mediator.Send(updateTraveler))
				{
					return Ok();
				}
				meta = new
				{
					Error = new { Code = 103, Description = ErrorMessageService.GetErrorMessage("103", language) }
				};
				return BadRequest(new { Meta = meta });
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				meta = new { Error = new { Code = 100, Description = ErrorMessageService.GetErrorMessage("100", language) } };
				return StatusCode(500, new { Meta = meta });
			}
		}

		[HttpDelete, Route("/Traveler/{TravelerId}")]
		public async Task<IActionResult> DeleteTraveler([FromRoute] string TravelerId, [FromQuery] string language)
		{
			Object meta;
			try
			{
				if (!ModelState.IsValid)
				{
					List<string> errors = new List<string>();
					foreach (KeyValuePair<string, ModelStateEntry> state in ModelState)
					{
						foreach (ModelError error in state.Value.Errors)
						{
							errors.Add(error.ErrorMessage);
						}
					}
					meta = new
					{
						Error = new { Code = 101, Description = ErrorMessageService.GetErrorMessage("101", language), Data = errors }
					};
					return BadRequest(new { Meta = meta });
				}

				TravelerDTO Traveler = await Mediator.Send(new GetTravelerById(TravelerId));
				if (Traveler == null)
				{
					meta = new
					{
						Error = new { Code = 401, Description = ErrorMessageService.GetErrorMessage("401", language) }
					};
					return BadRequest(new { Meta = meta });
				}

				DeleteTraveler newTraveler = new DeleteTraveler(
					Traveler.Id
				);
				if (await Mediator.Send(newTraveler))
				{
					return Ok();
				}
				meta = new
				{
					Error = new { Code = 103, Description = ErrorMessageService.GetErrorMessage("103", language) }
				};
				return BadRequest(new { Meta = meta });
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				meta = new { Error = new { Code = 100, Description = ErrorMessageService.GetErrorMessage("100", language) } };
				return StatusCode(500, new { Meta = meta });
			}
		}


		[HttpGet, Route("/Traveler")]
		public async Task<IActionResult> GetTravelers([FromQuery] string language)
		{
			Object meta;
			try
			{
				List<TravelerDTO> Travelers = await Mediator.Send(new GetTravelers());
				return Ok(Travelers);
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				meta = new { Error = new { Code = 100, Description = ErrorMessageService.GetErrorMessage("100", language) } };
				return StatusCode(500, new { Meta = meta });
			}
		}

		[HttpGet, Route("/Traveler/{travelId}")]
		public async Task<IActionResult> getTraveler([FromRoute] string travelId, [FromQuery] string language)
		{
			Object meta;
			try
			{

				TravelerDTO travel = await Mediator.Send(new GetTravelerById(travelId));
				if (travel == null)
				{
					return NotFound();
				}

				return Ok(travel);
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				meta = new { Error = new { Code = 100, Description = ErrorMessageService.GetErrorMessage("100", language) } };
				return StatusCode(500, new { Meta = meta });
			}
		}

	}
}
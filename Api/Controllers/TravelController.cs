using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Commands;
using Api.DTOs;
using Api.Models;
using Api.Queries;
using Api.Services;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Serilog;

namespace Api.Controllers
{

	[Produces("application/json")]
	public class TravelController : ControllerBase
	{
		private readonly ILogger Logger;
		private readonly ErrorMessageService ErrorMessageService;
		private readonly IMapper Mapper;
		private readonly IMediator Mediator;

		public TravelController(ILogger iLogger, ErrorMessageService errorMessageService, IMapper mapper, IMediator mediator)
		{
			Logger = iLogger;
			ErrorMessageService = errorMessageService;
			Mapper = mapper;
			Mediator = mediator;
		}

		[HttpPost, Route("/Travel")]
		public async Task<IActionResult> CreateTravel([FromBody] NewTravelModel model, [FromQuery] string language)
		{
			Object meta;
			try
			{

				if (!ModelState.IsValid)
				{
					List<string> errors = new List<string>();
					foreach (KeyValuePair<string, ModelStateEntry> state in ModelState)
					{
						foreach (ModelError error in state.Value.Errors)
						{
							errors.Add(error.ErrorMessage);
						}
					}
					meta = new
					{
						Error = new { Code = 101, Description = ErrorMessageService.GetErrorMessage("101", language), Data = errors }
					};
					return BadRequest(new { Meta = meta });
				}

				CreateTravel newTravel = new CreateTravel(model.Origin, model.Destination, model.Seats, model.Price);
				if (await Mediator.Send(newTravel))
				{
					return Ok();
				}
				meta = new
				{
					Error = new { Code = 103, Description = ErrorMessageService.GetErrorMessage("103", language) }
				};
				return BadRequest(new { Meta = meta });
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				meta = new { Error = new { Code = 100, Description = ErrorMessageService.GetErrorMessage("100", language) } };
				return StatusCode(500, new { Meta = meta });
			}
		}

		[HttpPut, Route("/Travel/{TravelId}")]
		public async Task<IActionResult> UpdateTravel([FromRoute] string TravelId, [FromBody] UpdateTravelModel model, [FromQuery] string language)
		{
			Object meta;
			try
			{
				if (!ModelState.IsValid)
				{
					List<string> errors = new List<string>();
					foreach (KeyValuePair<string, ModelStateEntry> state in ModelState)
					{
						foreach (ModelError error in state.Value.Errors)
						{
							errors.Add(error.ErrorMessage);
						}
					}
					meta = new
					{
						Error = new { Code = 101, Description = ErrorMessageService.GetErrorMessage("101", language), Data = errors }
					};
					return BadRequest(new { Meta = meta });
				}

				TravelDTO Travel = await Mediator.Send(new GetTravelById(TravelId));
				if (Travel == null)
				{
					meta = new
					{
						Error = new { Code = 301, Description = ErrorMessageService.GetErrorMessage("301", language) }
					};
					return BadRequest(new { Meta = meta });
				}

				UpdateTravel updateTravel = new UpdateTravel(
					TravelId,
					model.Origin,
					model.Destination,
					model.Seats,
					model.Price
				);

				if (await Mediator.Send(updateTravel))
				{
					return Ok();
				}
				meta = new
				{
					Error = new { Code = 103, Description = ErrorMessageService.GetErrorMessage("103", language) }
				};
				return BadRequest(new { Meta = meta });
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				meta = new { Error = new { Code = 100, Description = ErrorMessageService.GetErrorMessage("100", language) } };
				return StatusCode(500, new { Meta = meta });
			}
		}

		[HttpDelete, Route("/Travel/{TravelId}")]
		public async Task<IActionResult> DeleteTravel([FromRoute] string TravelId, [FromQuery] string language)
		{
			Object meta;
			try
			{
				if (!ModelState.IsValid)
				{
					List<string> errors = new List<string>();
					foreach (KeyValuePair<string, ModelStateEntry> state in ModelState)
					{
						foreach (ModelError error in state.Value.Errors)
						{
							errors.Add(error.ErrorMessage);
						}
					}
					meta = new
					{
						Error = new { Code = 101, Description = ErrorMessageService.GetErrorMessage("101", language), Data = errors }
					};
					return BadRequest(new { Meta = meta });
				}

				TravelDTO Travel = await Mediator.Send(new GetTravelById(TravelId));
				if (Travel == null)
				{
					meta = new
					{
						Error = new { Code = 301, Description = ErrorMessageService.GetErrorMessage("301", language) }
					};
					return BadRequest(new { Meta = meta });
				}

				DeleteTravel newTravel = new DeleteTravel(
					Travel.Id
				);
				if (await Mediator.Send(newTravel))
				{
					return Ok();
				}
				meta = new
				{
					Error = new { Code = 103, Description = ErrorMessageService.GetErrorMessage("103", language) }
				};
				return BadRequest(new { Meta = meta });
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				meta = new { Error = new { Code = 100, Description = ErrorMessageService.GetErrorMessage("100", language) } };
				return StatusCode(500, new { Meta = meta });
			}
		}


		[HttpGet, Route("/Travel")]
		public async Task<IActionResult> GetTravels([FromQuery] string language)
		{
			Object meta;
			try
			{
				List<TravelDTO> Travels = await Mediator.Send(new GetTravels());
				return Ok(Travels);
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				meta = new { Error = new { Code = 100, Description = ErrorMessageService.GetErrorMessage("100", language) } };
				return StatusCode(500, new { Meta = meta });
			}
		}

		[HttpGet, Route("/Travel/{travelId}")]
		public async Task<IActionResult> getTravel([FromRoute] string travelId, [FromQuery] string language)
		{
			Object meta;
			try
			{

				TravelDTO travel = await Mediator.Send(new GetTravelById(travelId));
				if (travel == null)
				{
					return NotFound();
				}

				return Ok(travel);
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				meta = new { Error = new { Code = 100, Description = ErrorMessageService.GetErrorMessage("100", language) } };
				return StatusCode(500, new { Meta = meta });
			}
		}

	}
}
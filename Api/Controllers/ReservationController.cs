using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Commands;
using Api.DTOs;
using Api.Models;
using Api.Queries;
using Api.Services;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Serilog;

namespace Api.Controllers
{

	[Produces("application/json")]
	public class ReservationTravelController : ControllerBase
	{
		private readonly ILogger Logger;
		private readonly ErrorMessageService ErrorMessageService;
		private readonly IMapper Mapper;
		private readonly IMediator Mediator;

		public ReservationTravelController(ILogger iLogger, ErrorMessageService errorMessageService, IMapper mapper, IMediator mediator)
		{
			Logger = iLogger;
			ErrorMessageService = errorMessageService;
			Mapper = mapper;
			Mediator = mediator;
		}

		[HttpPost, Route("/Reservation")]
		public async Task<IActionResult> CreateReservation([FromBody] NewReservationModel model, [FromQuery] string language)
		{
			Object meta;
			try
			{
				if (!ModelState.IsValid)
				{
					List<string> errors = new List<string>();
					foreach (KeyValuePair<string, ModelStateEntry> state in ModelState)
					{
						foreach (ModelError error in state.Value.Errors)
						{
							errors.Add(error.ErrorMessage);
						}
					}
					meta = new
					{
						Error = new { Code = 101, Description = ErrorMessageService.GetErrorMessage("101", language), Data = errors }
					};
					return BadRequest(new { Meta = meta });
				}

				CreateReservation newReservation = new CreateReservation(model.TravelerId.ToString(), model.TravelId.ToString());
				if (await Mediator.Send(newReservation))
				{
					return Ok();
				}
				meta = new
				{
					Error = new { Code = 103, Description = ErrorMessageService.GetErrorMessage("103", language) }
				};
				return BadRequest(new { Meta = meta });
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				meta = new { Error = new { Code = 100, Description = ErrorMessageService.GetErrorMessage("100", language) } };
				return StatusCode(500, new { Meta = meta });
			}
		}

		[HttpPut, Route("/Reservation/{reservationId}")]
		public async Task<IActionResult> UpdateReservation([FromRoute] string reservationId, [FromBody] EditedReservationModel model, [FromQuery] string language)
		{
			Object meta;
			try
			{
				if (!ModelState.IsValid)
				{
					List<string> errors = new List<string>();
					foreach (KeyValuePair<string, ModelStateEntry> state in ModelState)
					{
						foreach (ModelError error in state.Value.Errors)
						{
							errors.Add(error.ErrorMessage);
						}
					}
					meta = new
					{
						Error = new { Code = 101, Description = ErrorMessageService.GetErrorMessage("101", language), Data = errors }
					};
					return BadRequest(new { Meta = meta });
				}

				ReservationDTO reservation = await Mediator.Send(new GetReservationById(reservationId));
				if (reservation == null)
				{
					meta = new
					{
						Error = new { Code = 501, Description = ErrorMessageService.GetErrorMessage("501", language) }
					};
					return BadRequest(new { Meta = meta });
				}

				ReservationDTO existReservation = await Mediator.Send(
														new GetReservationByTravelIdAndTravelerId(
															model.TravelId.ToString(), 
															model.TravelerId.ToString()
															)
														);
				if (existReservation != null)
				{
					if (existReservation.Id != reservation.Id)
					{
						meta = new
						{
							Error = new { Code = 500, Description = ErrorMessageService.GetErrorMessage("500", language) }
						};
						return BadRequest(new { Meta = meta });
					}
					return Ok();
					
				}

				UpdateReservation updateReservation = new UpdateReservation(
					reservation.Id,
					model.TravelerId.ToString(),
					model.TravelId.ToString()
				);

				if (await Mediator.Send(updateReservation))
				{
					return Ok();
				}
				meta = new
				{
					Error = new { Code = 103, Description = ErrorMessageService.GetErrorMessage("103", language) }
				};
				return BadRequest(new { Meta = meta });
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				meta = new { Error = new { Code = 100, Description = ErrorMessageService.GetErrorMessage("100", language) } };
				return StatusCode(500, new { Meta = meta });
			}
		}

		[HttpDelete, Route("/Reservation/{ReservationId}")]
		public async Task<IActionResult> DeleteReservation([FromRoute] string ReservationId, [FromQuery] string language)
		{
			Object meta;
			try
			{
				if (!ModelState.IsValid)
				{
					List<string> errors = new List<string>();
					foreach (KeyValuePair<string, ModelStateEntry> state in ModelState)
					{
						foreach (ModelError error in state.Value.Errors)
						{
							errors.Add(error.ErrorMessage);
						}
					}
					meta = new
					{
						Error = new { Code = 101, Description = ErrorMessageService.GetErrorMessage("101", language), Data = errors }
					};
					return BadRequest(new { Meta = meta });
				}

				ReservationDTO Reservation = await Mediator.Send(new GetReservationById(ReservationId));
				if (Reservation == null)
				{
					meta = new
					{
						Error = new { Code = 501, Description = ErrorMessageService.GetErrorMessage("501", language) }
					};
					return BadRequest(new { Meta = meta });
				}

				DeleteReservation newReservation = new DeleteReservation(
					Reservation.Id
				);
				if (await Mediator.Send(newReservation))
				{
					return Ok();
				}
				meta = new
				{
					Error = new { Code = 103, Description = ErrorMessageService.GetErrorMessage("103", language) }
				};
				return BadRequest(new { Meta = meta });
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				meta = new { Error = new { Code = 100, Description = ErrorMessageService.GetErrorMessage("100", language) } };
				return StatusCode(500, new { Meta = meta });
			}
		}

		[HttpGet, Route("/Travel/{travelId}/Reservation")]
		public async Task<IActionResult> GetReservationsByTravel([FromRoute] string travelId, [FromQuery] string language)
		{
			Object meta;
			try
			{
				List<ReservationDTO> reservations = await Mediator.Send(new GetReservationsByTravelId(travelId));
				return Ok(reservations);
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				meta = new { Error = new { Code = 100, Description = ErrorMessageService.GetErrorMessage("100", language) } };
				return StatusCode(500, new { Meta = meta });
			}
		}


		[HttpGet, Route("/Traveler/{travelerId}/Reservation")]
		public async Task<IActionResult> GetReservationsByTraveler([FromRoute] string travelerId, [FromQuery] string language)
		{
			Object meta;
			try
			{
				List<ReservationDTO> reservations = await Mediator.Send(new GetReservationsByTravelerId(travelerId));
				return Ok(reservations);
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				meta = new { Error = new { Code = 100, Description = ErrorMessageService.GetErrorMessage("100", language) } };
				return StatusCode(500, new { Meta = meta });
			}
		}

		[HttpGet, Route("/Reservation/{id}")]
		public async Task<IActionResult> getReservation([FromRoute] string id, [FromQuery] string language)
		{
			Object meta;
			try
			{

				ReservationDTO reservation = await Mediator.Send(new GetReservationById(id));
				if (reservation == null)
				{
					return NotFound();
				}

				return Ok(reservation);
			}
			catch (Exception exception)
			{
				Logger.Fatal(exception.Message);
				Logger.Fatal(exception.StackTrace);
				meta = new { Error = new { Code = 100, Description = ErrorMessageService.GetErrorMessage("100", language) } };
				return StatusCode(500, new { Meta = meta });
			}
		}
	}
}
using AutoMapper;
using Api.DTOs;
using Api.Entities;

namespace Api.MapperProfiles
{
	public class Profiles : Profile
	{
		public Profiles()
		{

			CreateMap<Travel, TravelDTO>()
				.ForMember(destination => destination.Id, origin => origin.MapFrom(source => source.TravelId));

			CreateMap<Traveler, TravelerDTO>()
				.ForMember(destination => destination.Id, origin => origin.MapFrom(source => source.TravelerId));

            CreateMap<Reservation, ReservationDTO>()
                .ForMember(destination => destination.Id, origin => origin.MapFrom(source => source.ReservationId));

		}
	}
}

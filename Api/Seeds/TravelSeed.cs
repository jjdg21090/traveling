using MediatR;
using Api.Commands;
using System.Threading.Tasks;

namespace Api.Seeds
{
	public class TravelSeed
	{
		private readonly IMediator Mediator;

		public TravelSeed(IMediator mediator)
		{
			Mediator = mediator;
		}

		public async Task Run()
		{
			string origin = "Caracas";
			string destination = "Barquisimeto";
			int seats = 75;
			double price = 35.0;

			CreateTravel travel = new CreateTravel(
				origin,
				destination,
				seats,
				price
			);
			await Mediator.Send(travel);
		}
	}
}

using System;
using Api.Entities;
using Microsoft.EntityFrameworkCore;

namespace Api
{
	public class PgsqlContext : DbContext
	{
		public DbSet<Traveler> Traveler { get; set; }
		public DbSet<Travel> Travel { get; set; }
		public DbSet<Reservation> Reservation { get; set; }
		public PgsqlContext(DbContextOptions<PgsqlContext> options) : base(options) { }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Reservation>()
				.HasAlternateKey(tt => new { tt.TravelerId, tt.TravelId });
			modelBuilder.Entity<Reservation>()
				.HasOne(tt => tt.Traveler)
				.WithMany(t1 => t1.Reservations)
				.HasForeignKey(bc => bc.TravelerId)
				.OnDelete(DeleteBehavior.Cascade)
				.IsRequired();
			modelBuilder.Entity<Reservation>()
				.HasOne(tt => tt.Travel)
				.WithMany(t2 => t2.Reservations)
				.HasForeignKey(bc => bc.TravelId)
				.OnDelete(DeleteBehavior.Cascade)
				.IsRequired();
		}
	}
}

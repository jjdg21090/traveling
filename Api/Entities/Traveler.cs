using System;
using System.Collections.Generic;

namespace Api.Entities
{
	public class Traveler
	{
		public Guid TravelerId { get; set; }
		public string Name { get; set; }
		public string DocumentId { get; set; }
		public string Address { get; set; }
		public string Phone { get; set; }
		public ICollection<Reservation> Reservations { get; set; }

	}
}
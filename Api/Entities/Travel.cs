using System;
using System.Collections.Generic;

namespace Api.Entities
{
	public class Travel
	{
		public Guid TravelId { get; set; }
		public int Seats { get; set; }
		public string Origin { get; set; }
		public string Destination { get; set; }
		public double Price { get; set; }
		public ICollection<Reservation> Reservations { get; set; }

	}
}
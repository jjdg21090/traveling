using System;
using System.ComponentModel.DataAnnotations;

namespace Api.Entities
{
	public class Reservation
	{
		[Key]
		public Guid ReservationId { get; set; }
		public Guid TravelerId { get; set; }
		public Traveler Traveler { get; set; }
		public Guid TravelId { get; set; }
		public Travel Travel { get; set; }
	}
}
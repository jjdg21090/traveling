namespace Api.DTOs
{
	public class TravelerDTO
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string DocumentId { get; set; }
		public string Address { get; set; }
		public string Phone { get; set; }

	}
}
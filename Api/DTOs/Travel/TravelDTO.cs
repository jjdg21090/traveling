namespace Api.DTOs
{
	public class TravelDTO
	{
		public string Id { get; set; }
		public string Origin { get; set; }
		public string Destination { get; set; }
		public int Seats { get; set; }
		public double Price { get; set; }

	}
}
namespace Api.DTOs
{
	public class ReservationDTO
	{
		public string Id { get; set; }
		public TravelerDTO Traveler { get; set; }
		public string TravelerId { get; set; }
		public TravelDTO Travel { get; set; }
		public string TravelId { get; set; }
	}
}
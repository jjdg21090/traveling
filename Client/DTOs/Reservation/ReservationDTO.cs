namespace Client.DTOs
{
	public class ReservationDTO
	{
		public string Id { get; set; }
		public string TravelerId { get; set; }
		public string TravelId { get; set; }
	}
}
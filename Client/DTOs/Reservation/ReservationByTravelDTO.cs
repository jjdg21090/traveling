using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Client.DTOs
{
	public class GetReservationByTravelDTO
	{
		public string Id { get; set; }
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		public TravelDTO Travel { get; set; }
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		public IEnumerable<TravelerDTO> Travelers { get; set; }
	}
}

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Client.DTOs
{
	public class GetReservationByTravelerDTO
	{
		public string Id { get; set; }
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		public TravelerDTO Traveler { get; set; }
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		public IEnumerable<TravelDTO> Travels { get; set; }
	}
}

namespace Client.DTOs
{
	public class ReservationListDTO
	{
		public string Id { get; set; }
		public TravelerDTO Traveler { get; set; }
		public TravelDTO Travel { get; set; }
	}
}
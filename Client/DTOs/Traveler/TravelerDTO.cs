using System.ComponentModel.DataAnnotations;

namespace Client.DTOs
{
	public class TravelerDTO
	{
		public string Id { get; set; }
		[Display(Name = "Nombre")]
		public string Name { get; set; }
		[Display(Name = "Cédula")]
		public string DocumentId { get; set; }
		[Display(Name = "Dirección")]
		public string Address { get; set; }
		[Display(Name = "Teléfono")]
		public string Phone { get; set; }

	}
}
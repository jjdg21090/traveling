using System.ComponentModel.DataAnnotations;

namespace Client.DTOs
{
	public class DeleteTravelDTO
	{
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		public string Id { get; set; }
	}
}

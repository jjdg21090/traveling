using System.ComponentModel.DataAnnotations;

namespace Client.DTOs
{
	public class TravelDTO
	{
		public string Id { get; set; }
		[Display(Name = "Lugar de origen")]
		public string Origin { get; set; }
		[Display(Name = "Destino")]
		public string Destination { get; set; }
		[Display(Name = "Plazas")]
		public int Seats { get; set; }
		[Display(Name = "Precio")]
		public double Price { get; set; }
		[Display(Name = "Ruta")]
		public string Route { get { return Origin + " - " + Destination; } }

	}
}
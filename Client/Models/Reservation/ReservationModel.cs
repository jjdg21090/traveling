using System.ComponentModel.DataAnnotations;

namespace Client.Models
{
	public class ReservationModel
	{
		public string Id { get; set; }
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		[Display(Name = "Pasajero")]
		public string TravelerId { get; set; }
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		[Display(Name = "Viaje")]
		public string TravelId { get; set; }
	}
}

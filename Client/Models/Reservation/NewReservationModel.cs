using System.ComponentModel.DataAnnotations;

namespace Client.Models
{
	public class NewReservationModel
	{
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		[Display(Name = "Viaje")]
		public string TravelId { get; set; }
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		[Display(Name = "Pasajero")]
		public string TravelerId { get; set; }
	}
}

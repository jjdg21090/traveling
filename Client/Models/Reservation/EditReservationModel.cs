using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Client.Models
{
	public class EditReservationModel
	{
		public string ReservationId { get; set; }
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		[Display(Name = "Viaje")]
		public string TravelId { get; set; }
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		[Display(Name = "Pasajero")]
		public string TravelerId { get; set; }
	}
}

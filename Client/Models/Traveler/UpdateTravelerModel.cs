using System.ComponentModel.DataAnnotations;

namespace Client.Models
{
	public class UpdateTravelerModel
	{
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		[Display(Name = "Nombre")]
		public string Name { get; set; }
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		[Display(Name = "Cédula")]
		public string DocumentId { get; set; }
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		[Display(Name = "Dirección")]
		public string Address { get; set; }
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		[Display(Name = "Teléfono")]
		public string Phone { get; set; }
	}
}

using System.ComponentModel.DataAnnotations;

namespace Client.Models
{
	public class NewTravelModel
	{
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		[Display(Name = "Lugar de origen")]
		public string Origin { get; set; }
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		[Display(Name = "Destino")]
		public string Destination { get; set; }
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		[Display(Name = "Plazas")]
		public int Seats { get; set; }
		[Required(ErrorMessage = "Este campo es obligatorio.")]
		[Display(Name = "Precio")]
		public double Price { get; set; }
	}
}

using AutoMapper;
using Client.DTOs;
using Client.Models;
using Newtonsoft.Json.Linq;

namespace Client.MapperProfiles
{
	public class Profiles : Profile
	{
		public Profiles()
		{

			CreateMap<JObject, TravelDTO>()
				.ForMember(destination => destination.Id, origin => origin.MapFrom(source => source.SelectToken(".id")))
				.ForMember(destination => destination.Origin, origin => origin.MapFrom(source => source.SelectToken(".origin")))
				.ForMember(destination => destination.Destination, origin => origin.MapFrom(source => source.SelectToken(".destination")))
				.ForMember(destination => destination.Seats, origin => origin.MapFrom(source => source.SelectToken(".seats")))
				.ForMember(destination => destination.Price, origin => origin.MapFrom(source => source.SelectToken(".price")));

			CreateMap<JToken, TravelDTO>()
				.ForMember(destination => destination.Id, origin => origin.MapFrom(source => source.SelectToken(".id")))
				.ForMember(destination => destination.Origin, origin => origin.MapFrom(source => source.SelectToken(".origin")))
				.ForMember(destination => destination.Destination, origin => origin.MapFrom(source => source.SelectToken(".destination")))
				.ForMember(destination => destination.Seats, origin => origin.MapFrom(source => source.SelectToken(".seats")))
				.ForMember(destination => destination.Price, origin => origin.MapFrom(source => source.SelectToken(".price")));

			CreateMap<JObject, TravelModel>()
				.ForMember(destination => destination.Id, origin => origin.MapFrom(source => source.SelectToken(".id")))
				.ForMember(destination => destination.Origin, origin => origin.MapFrom(source => source.SelectToken(".origin")))
				.ForMember(destination => destination.Destination, origin => origin.MapFrom(source => source.SelectToken(".destination")))
				.ForMember(destination => destination.Seats, origin => origin.MapFrom(source => source.SelectToken(".seats")))
				.ForMember(destination => destination.Price, origin => origin.MapFrom(source => source.SelectToken(".price")));

			CreateMap<JObject, TravelerDTO>()
				.ForMember(destination => destination.Id, origin => origin.MapFrom(source => source.SelectToken(".id")))
				.ForMember(destination => destination.Name, origin => origin.MapFrom(source => source.SelectToken(".name")))
				.ForMember(destination => destination.DocumentId, origin => origin.MapFrom(source => source.SelectToken(".documentId")))
				.ForMember(destination => destination.Address, origin => origin.MapFrom(source => source.SelectToken(".address")))
				.ForMember(destination => destination.Phone, origin => origin.MapFrom(source => source.SelectToken(".phone")));

			CreateMap<JToken, TravelerDTO>()
				.ForMember(destination => destination.Id, origin => origin.MapFrom(source => source.SelectToken(".id")))
				.ForMember(destination => destination.Name, origin => origin.MapFrom(source => source.SelectToken(".name")))
				.ForMember(destination => destination.DocumentId, origin => origin.MapFrom(source => source.SelectToken(".documentId")))
				.ForMember(destination => destination.Address, origin => origin.MapFrom(source => source.SelectToken(".address")))
				.ForMember(destination => destination.Phone, origin => origin.MapFrom(source => source.SelectToken(".phone")));

			CreateMap<JObject, TravelerModel>()
				.ForMember(destination => destination.Id, origin => origin.MapFrom(source => source.SelectToken(".id")))
				.ForMember(destination => destination.Name, origin => origin.MapFrom(source => source.SelectToken(".name")))
				.ForMember(destination => destination.DocumentId, origin => origin.MapFrom(source => source.SelectToken(".documentId")))
				.ForMember(destination => destination.Address, origin => origin.MapFrom(source => source.SelectToken(".address")))
				.ForMember(destination => destination.Phone, origin => origin.MapFrom(source => source.SelectToken(".phone")));

			CreateMap<JObject, ReservationDTO>()
				.ForMember(destination => destination.Id, origin => origin.MapFrom(source => source.SelectToken(".id")))
				.ForMember(destination => destination.TravelerId, origin => origin.MapFrom(source => source.SelectToken(".travelerId")))
				.ForMember(destination => destination.TravelId, origin => origin.MapFrom(source => source.SelectToken(".travelId")));

			CreateMap<JObject, ReservationListDTO>()
				.ForMember(destination => destination.Id, origin => origin.MapFrom(source => source.SelectToken(".id")))
				.ForMember(destination => destination.Traveler, origin => origin.MapFrom(source => source.SelectToken(".traveler")))
				.ForMember(destination => destination.Travel, origin => origin.MapFrom(source => source.SelectToken(".travel")));
		}
	}
}

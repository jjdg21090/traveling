using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Client.DTOs;
using Client.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;

namespace Client.Controllers
{
	public class TravelController : Controller
	{
		public readonly ILogger Logger;
		private readonly IMapper Mapper;

		public readonly IConfiguration Configure;

		public readonly string ApiBaseUrl;

		public TravelController(ILogger logger, IMapper mapper, IConfiguration configuration)
		{
			Logger = logger;
			Mapper = mapper;
			Configure = configuration;

			ApiBaseUrl = Configure.GetValue<string>("WebAPIBaseUrl");
		}

		// GET: Travel
		[HttpGet]
		public async Task<ActionResult> Index()
		{
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Travel";

				using (var response = await client.GetAsync(endpoint))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}

					string apiResponse = await response.Content.ReadAsStringAsync();
					
					List<JObject> dataObject = JsonConvert.DeserializeObject<List<JObject>>(apiResponse);
					List<TravelDTO> travels = Mapper.Map<List<TravelDTO>>(dataObject);

					return View(travels);
				}
			}
		}

		// GET: Travel/Create
		public IActionResult Create()
		{
			return View();
		}

		// POST: Travel/Create
		[Produces("application/json")]
		[HttpPost]
		public async Task<IActionResult> Create([Bind("Origin,Destination,Seats,Price")] NewTravelModel travel)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(travel);
			}
			List<TravelDTO> travels = null;
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Travel";

				var jsonContent = new StringContent(JsonConvert.SerializeObject(travel), Encoding.UTF8, "application/json");

				using (var response = await client.PostAsync(endpoint, jsonContent))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}

					string apiResponse = await response.Content.ReadAsStringAsync();

					travels = JsonConvert.DeserializeObject<List<TravelDTO>>(apiResponse);

					return Ok();
				}
			}
		}

		// GET: Travel/Edit/5
		[HttpGet, Route("/Travel/Edit/{id}")]
		public async Task<IActionResult> Edit(Guid id)
		{
			if (id == null)
			{
				return BadRequest();
			}
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Travel/{id}";

				using (var response = await client.GetAsync(endpoint))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}

					string apiResponse = await response.Content.ReadAsStringAsync();

					TravelDTO travel = JsonConvert.DeserializeObject<TravelDTO>(apiResponse);

					return View(travel);
				}
			}
		}

		// Put: Travel/Edit/5
		[Produces("application/json")]
		[HttpPut, Route("/Travel/Edit/{id}")]
		public async Task<IActionResult> Edit(Guid id, [Bind("Id,Origin,Destination,Seats,Price")] TravelModel travel)
		{
			if (id.ToString() != travel.Id)
			{
				return BadRequest(travel);
			}
			if (!ModelState.IsValid)
			{
				return BadRequest(travel);
			}
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Travel/{travel.Id}";

				StringContent jsonContent = new StringContent(JsonConvert.SerializeObject(travel), Encoding.UTF8, "application/json");

				using (var response = await client.PutAsync(endpoint, jsonContent))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}

					string endpointGetTravel = $@"{ApiBaseUrl}/Travel";

					using (var responseGetTravel = await client.GetAsync(endpointGetTravel))
					{
						if (!response.IsSuccessStatusCode)
						{
							return StatusCode((int)response.StatusCode);
						}

						string apiResponseGetTravel = await responseGetTravel.Content.ReadAsStringAsync();

						List<JObject> dataObject = JsonConvert.DeserializeObject<List<JObject>>(apiResponseGetTravel);
						List<TravelDTO> travels = Mapper.Map<List<TravelDTO>>(dataObject);

						return PartialView("_TravelListPartial", travels);
					}
				}
			}
		}

		// GET: Travel/Edit/5
		[HttpGet, Route("/Travel/Details/{id}")]
		public async Task<IActionResult> Details(Guid id)
		{
			if (id == null)
			{
				return BadRequest();
			}
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Travel/{id}";

				using (var response = await client.GetAsync(endpoint))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}

					string apiResponse = await response.Content.ReadAsStringAsync();

					TravelDTO travel = JsonConvert.DeserializeObject<TravelDTO>(apiResponse);

					return View(travel);
				}
			}
		}

		// Delete: Travel/Delete/5
		[Produces("application/json")]
		[HttpDelete, Route("/Travel/delete/{id}")]
		public async Task<IActionResult> DeleteConfirmed(Guid id)
		{
			if (id == null)
			{
				return BadRequest();
			}
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Travel/{id}";

				using (var response = await client.DeleteAsync(endpoint))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}

					string endpointGetTravel = $@"{ApiBaseUrl}/Travel";

					using (var responseGetTravel = await client.GetAsync(endpointGetTravel))
					{
						if (!response.IsSuccessStatusCode)
						{
							return StatusCode((int)response.StatusCode);
						}

						string apiResponseGetTravel = await responseGetTravel.Content.ReadAsStringAsync();

						List<JObject> dataObject = JsonConvert.DeserializeObject<List<JObject>>(apiResponseGetTravel);
						List<TravelDTO> travels = Mapper.Map<List<TravelDTO>>(dataObject);

						return PartialView("_TravelListPartial", travels);
					}
				}
			}
		}
	}
}
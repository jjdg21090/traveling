using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Client.DTOs;
using Client.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;

namespace Client.Controllers
{
	public class TravelerController : Controller
	{
		public readonly ILogger Logger;
		private readonly IMapper Mapper;

		public readonly IConfiguration Configure;

		public readonly string ApiBaseUrl;

		public TravelerController(ILogger logger, IMapper mapper, IConfiguration configuration)
		{
			Logger = logger;
			Mapper = mapper;
			Configure = configuration;

			ApiBaseUrl = Configure.GetValue<string>("WebAPIBaseUrl");
		}

		// GET: Traveler
		[HttpGet]
		public async Task<ActionResult> Index()
		{
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Traveler";

				using (var response = await client.GetAsync(endpoint))
				{
					if (response.IsSuccessStatusCode)
					{
						string apiResponse = await response.Content.ReadAsStringAsync();

						List<JObject> dataObject = JsonConvert.DeserializeObject<List<JObject>>(apiResponse);
						List<TravelerDTO> travelers = Mapper.Map<List<TravelerDTO>>(dataObject);

						return View(travelers);
					}
					return StatusCode((int)response.StatusCode);
				}
			}
		}

		// GET: Traveler/Create
		public IActionResult Create()
		{
			return View();
		}

		// POST: Traveler/Create
		[Produces("application/json")]
		[HttpPost]
		public async Task<IActionResult> Create([Bind("Name,DocumentId,Address,Phone")] NewTravelerModel traveler)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(traveler);
			}
			List<TravelerDTO> travelers = null;
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Traveler";

				var jsonContent = new StringContent(JsonConvert.SerializeObject(traveler), Encoding.UTF8, "application/json");

				using (var response = await client.PostAsync(endpoint, jsonContent))
				{
					if (response.IsSuccessStatusCode)
					{
						string apiResponse = await response.Content.ReadAsStringAsync();

						travelers = JsonConvert.DeserializeObject<List<TravelerDTO>>(apiResponse);

						return Ok();
					}
					return StatusCode((int)response.StatusCode);
				}
			}
		}

		// GET: Traveler/Edit/5
		[HttpGet, Route("/Traveler/Edit/{id}")]
		public async Task<IActionResult> Edit(Guid id)
		{
			if (id == null)
			{
				return BadRequest();
			}
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Traveler/{id}";

				using (var response = await client.GetAsync(endpoint))
				{
					if (response.IsSuccessStatusCode)
					{
						string apiResponse = await response.Content.ReadAsStringAsync();

						TravelerDTO traveler = JsonConvert.DeserializeObject<TravelerDTO>(apiResponse);

						return View(traveler);
					}
					return StatusCode((int)response.StatusCode);
				}
			}
		}

		// Put: Traveler/Edit/5
		[Produces("application/json")]
		[HttpPut, Route("/Traveler/Edit/{id}")]
		public async Task<IActionResult> Edit(Guid id, [Bind("Id,Name,DocumentId,Address,Phone")] TravelerModel traveler)
		{
			if (id.ToString() != traveler.Id)
			{
				return BadRequest(traveler);
			}
			if (!ModelState.IsValid)
			{
				return BadRequest(traveler);
			}
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Traveler/{traveler.Id}";

				StringContent jsonContent = new StringContent(JsonConvert.SerializeObject(traveler), Encoding.UTF8, "application/json");

				using (var response = await client.PutAsync(endpoint, jsonContent))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}
					string endpointGetTraveler = $@"{ApiBaseUrl}/Traveler";

					using (var responseGetTraveler = await client.GetAsync(endpointGetTraveler))
					{
						if (!response.IsSuccessStatusCode)
						{
							return StatusCode((int)response.StatusCode);
						}
						string apiResponseGetTraveler = await responseGetTraveler.Content.ReadAsStringAsync();

						List<JObject> dataObject = JsonConvert.DeserializeObject<List<JObject>>(apiResponseGetTraveler);
						List<TravelerDTO> travelers = Mapper.Map<List<TravelerDTO>>(dataObject);

						return PartialView("_TravelerListPartial", travelers);
					}
				}
			}
		}

		// GET: Traveler/Edit/5
		[HttpGet, Route("/Traveler/Details/{id}")]
		public async Task<IActionResult> Details(Guid id)
		{
			if (id == null)
			{
				return BadRequest();
			}
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Traveler/{id}";

				using (var response = await client.GetAsync(endpoint))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}

					string apiResponse = await response.Content.ReadAsStringAsync();

					TravelerDTO traveler = JsonConvert.DeserializeObject<TravelerDTO>(apiResponse);

					return View(traveler);
				}
			}
		}

		// Delete: Traveler/Delete/5
		[Produces("application/json")]
		[HttpDelete, Route("/Traveler/Delete/{id}")]
		public async Task<IActionResult> DeleteConfirmed(Guid id)
		{
			if (id == null)
			{
				return BadRequest();
			}
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Traveler/{id}";

				using (var response = await client.DeleteAsync(endpoint))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}

					string endpointGetTraveler = $@"{ApiBaseUrl}/Traveler";

					using (var responseGetTraveler = await client.GetAsync(endpointGetTraveler))
					{
						if (!response.IsSuccessStatusCode)
						{
							return StatusCode((int)response.StatusCode);
						}

						string apiResponseGetTraveler = await responseGetTraveler.Content.ReadAsStringAsync();

						List<JObject> dataObject = JsonConvert.DeserializeObject<List<JObject>>(apiResponseGetTraveler);
						List<TravelerDTO> travelers = Mapper.Map<List<TravelerDTO>>(dataObject);

						return PartialView("_TravelerListPartial", travelers);
					}
				}
			}
		}
	}
}
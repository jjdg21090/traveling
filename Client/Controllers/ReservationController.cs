using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Client.DTOs;
using Client.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;

namespace Client.Controllers
{
	public class ReservationController : Controller
	{
		public readonly ILogger Logger;
		private readonly IMapper Mapper;

		public readonly IConfiguration Configure;

		public readonly string ApiBaseUrl;

		public ReservationController(ILogger logger, IMapper mapper, IConfiguration configuration)
		{
			Logger = logger;
			Mapper = mapper;
			Configure = configuration;

			ApiBaseUrl = Configure.GetValue<string>("WebAPIBaseUrl");
		}

		// GET: Travel/Create
		[HttpGet]
		public async Task<IActionResult> Create()
		{
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Travel";
				NewReservationModel model = new NewReservationModel();

				using (var response = await client.GetAsync(endpoint))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}

					string apiResponse = await response.Content.ReadAsStringAsync();

					IEnumerable<TravelDTO> travels = JsonConvert.DeserializeObject<List<TravelDTO>>(apiResponse);

					ViewBag.ListTravels = new SelectList(travels, "Id", "Route");
				}

				string endpointTraveler = $@"{ApiBaseUrl}/Traveler";

				using (var response = await client.GetAsync(endpointTraveler))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}

					string apiResponse = await response.Content.ReadAsStringAsync();

					IEnumerable<TravelerDTO> travelers = JsonConvert.DeserializeObject<List<TravelerDTO>>(apiResponse);

					ViewBag.ListTravelers = new SelectList(travelers, "Id", "Name");
				}

				return View(model);
			}
		}

		// GET: /Reservation/Traveler/{travelerId}
		[HttpGet("/Reservation/Traveler/{travelerId}")]
		public async Task<ActionResult> GetReservationByTraveler(string travelerId)
		{
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Traveler/{travelerId}/Reservation";

				using (var response = await client.GetAsync(endpoint))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}

					string apiResponse = await response.Content.ReadAsStringAsync();

					List<JObject> dataObject = JsonConvert.DeserializeObject<List<JObject>>(apiResponse);
					List<ReservationListDTO> reservations = Mapper.Map<List<ReservationListDTO>>(dataObject);

					return View(reservations);
				}
			}
		}

		// GET: /Reservation/Travel/{travelId}
		[HttpGet("/Reservation/Travel/{travelId}")]
		public async Task<ActionResult> GetReservationByTravel(string travelId)
		{
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Travel/{travelId}/Reservation";

				using (var response = await client.GetAsync(endpoint))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}

					string apiResponse = await response.Content.ReadAsStringAsync();

					List<JObject> dataObject = JsonConvert.DeserializeObject<List<JObject>>(apiResponse);
					List<ReservationListDTO> reservations = Mapper.Map<List<ReservationListDTO>>(dataObject);

					return View(reservations);
				}
			}
		}

		// POST: /Traveler/{travelerId}/Reservation
		[Produces("application/json")]
		[HttpPost]
		public async Task<ActionResult> Create(Guid travelerId, [Bind("TravelId", "TravelerId")] NewReservationModel reservation)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(reservation);
			}
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Reservation";

				var jsonContent = new StringContent(JsonConvert.SerializeObject(reservation), Encoding.UTF8, "application/json");

				using (var response = await client.PostAsync(endpoint, jsonContent))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}

					string apiResponse = await response.Content.ReadAsStringAsync();

					List<ReservationDTO> reservations = JsonConvert.DeserializeObject<List<ReservationDTO>>(apiResponse);

					return Ok();
				}
			}
		}

		// GET: Reservation/Edit/5
		[HttpGet("/Reservation/Edit/{id}")]
		public async Task<IActionResult> Edit(Guid id)
		{
			if (id == null)
			{
				return BadRequest();
			}
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Reservation/{id}";
				EditReservationModel model = new EditReservationModel();

				using (var response = await client.GetAsync(endpoint))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}

					string apiResponse = await response.Content.ReadAsStringAsync();

					ReservationDTO reservation = JsonConvert.DeserializeObject<ReservationDTO>(apiResponse);

					model.ReservationId = id.ToString();
					model.TravelerId = reservation.TravelerId;
					model.TravelId = reservation.TravelId;
				}
				string endpointTravel = $@"{ApiBaseUrl}/Travel";

				using (var response = await client.GetAsync(endpointTravel))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}

					string apiResponse = await response.Content.ReadAsStringAsync();

					IEnumerable<TravelDTO> travels = JsonConvert.DeserializeObject<List<TravelDTO>>(apiResponse);

					ViewBag.ListTravels = new SelectList(travels, "Id", "Route");
				}

				string endpointTraveler = $@"{ApiBaseUrl}/Traveler";

				using (var response = await client.GetAsync(endpointTraveler))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}

					string apiResponse = await response.Content.ReadAsStringAsync();

					IEnumerable<TravelerDTO> travelers = JsonConvert.DeserializeObject<List<TravelerDTO>>(apiResponse);

					ViewBag.ListTravelers = new SelectList(travelers, "Id", "Name");
				}

				return View(model);
			}
		}

		// Put: Reservation/Edit/5
		[Produces("application/json")]
		[HttpPut("/Reservation/Edit/{id}")]
		public async Task<IActionResult> Edit(Guid id, [Bind("Id,TravelId,TravelerId")] ReservationModel updateReservation)
		{
			if (id.ToString() != updateReservation.Id)
			{
				return BadRequest(updateReservation);
			}
			if (!ModelState.IsValid)
			{
				return BadRequest(updateReservation);
			}
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Reservation/{updateReservation.Id}";

				StringContent jsonContent = new StringContent(JsonConvert.SerializeObject(updateReservation), Encoding.UTF8, "application/json");

				using (var response = await client.PutAsync(endpoint, jsonContent))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}
					return Ok();
				}
			}
		}

		// GET: Travel/Edit/5
		[HttpGet("/Reservation/{id}")]
		public async Task<IActionResult> Details(Guid id)
		{
			if (id == null)
			{
				return BadRequest();
			}
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Reservation/{id}";

				using (var response = await client.GetAsync(endpoint))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}

					string apiResponse = await response.Content.ReadAsStringAsync();

					ReservationDTO reservation = JsonConvert.DeserializeObject<ReservationDTO>(apiResponse);

					return View(reservation);
				}
			}
		}

		// Delete: Travel/Delete/5
		[Produces("application/json")]
		[HttpDelete("/Reservation/Delete/{id}")]
		public async Task<IActionResult> DeleteConfirmed(Guid id)
		{
			if (id == null)
			{
				return BadRequest();
			}
			using (HttpClient client = new HttpClient())
			{
				string endpoint = $@"{ApiBaseUrl}/Reservation/{id}";

				using (var response = await client.DeleteAsync(endpoint))
				{
					if (!response.IsSuccessStatusCode)
					{
						return StatusCode((int)response.StatusCode);
					}

					string endpointGetTravel = $@"{ApiBaseUrl}/Travel";

					using (var responseGetTravel = await client.GetAsync(endpointGetTravel))
					{
						if (!response.IsSuccessStatusCode)
						{
							return StatusCode((int)response.StatusCode);
						}

						string apiResponseGetTravel = await responseGetTravel.Content.ReadAsStringAsync();

						List<JObject> dataObject = JsonConvert.DeserializeObject<List<JObject>>(apiResponseGetTravel);
						List<TravelDTO> reservations = Mapper.Map<List<TravelDTO>>(dataObject);

						return PartialView("_ReservationListPartial", reservations);
					}
				}
			}
		}
	}
}